<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class AddPictureUrlField extends Migration
{
    public function up()
    {
        //
        if ($this->db->tableexists('Movie'))
        {
            $this->forge->addColumn('Movie',array(
                'poster_url' => array('type' => 'VARCHAR', 'constraint' => '255', 'null' => TRUE)
            ));
        }
    }

    public function down()
    {
        //
        $this->forge->dropColumn('Movie', 'poster_url');
    }
}
