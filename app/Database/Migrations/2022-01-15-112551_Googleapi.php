<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class Googleapi extends Migration
{
    public function up()
    {
        if ($this->db->tableexists('users'))
        {
            $this->forge->addcolumn('users', array(
                'google_id' => array('type' => 'VARCHAR', 'constraint' => '255', 'null' => TRUE),
                'locale' => array('type' => 'VARCHAR', 'constraint' => '255', 'null' => TRUE),
                'picture_url' => array('type' => 'VARCHAR', 'constraint' => '255', 'null' => TRUE))
            );
        }
        //
    }

    public function down()
    {
        $this->forge->dropcolumn('users', 'google_id');
        $this->forge->dropcolumn('users', 'locale');
        $this->forge->dropcolumn('users', 'picture_url');//
    }
}
