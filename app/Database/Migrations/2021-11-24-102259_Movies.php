<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class Movies extends Migration
{
    public function up()
    {
        // create Movie table
        if (!$this->db->tableexists('Movie'))//if don't exist same
        {
            // Setup Keys
            $this->forge->addkey('id', TRUE);

            //fields
            $this->forge->addfield(array(
                'id' => array('type' => 'INT', 'unsigned' => TRUE, 'null' => FALSE, 'auto_increment' => TRUE),
                'name' => array('type' => 'VARCHAR', 'constraint' => '255', 'null' => FALSE),
                'lasting' => array('type' => 'TIME', 'null' => TRUE),
            ));

            //create
            $this->forge->createtable('Movie', TRUE);
        }
        // create Zal table
        if (!$this->db->tableexists('Zal'))
        {
            // Setup Keys
            $this->forge->addkey('id', TRUE);

            $this->forge->addfield(array(
                'id' => array('type' => 'INT', 'unsigned' => TRUE, 'null' => FALSE, 'auto_increment' => TRUE),
                'name' => array('type' => 'VARCHAR', 'constraint' => '255', 'null' => FALSE),
            ));
            // create table
            $this->forge->createtable('Zal', TRUE);
        }

        //create session table
        if (!$this->db->tableexists('session'))
        {
            // Setup Keys
            $this->forge->addkey('id', TRUE);

            $this->forge->addfield(array(
                'id' => array('type' => 'INT', 'unsigned' => TRUE, 'null' => FALSE, 'auto_increment' => TRUE),
                'date' => array('type' => 'DATETIME', 'null' => FALSE),
                'zal_id' => array('type' => 'INT', 'unsigned' => TRUE),
                'movie_id' => array('type' => 'INT', 'unsigned' => TRUE)

            ));
            //add key bindings
            $this->forge->addForeignKey('zal_id','Zal','id','RESTRICT','RESRICT');
            $this->forge->addForeignKey('movie_id','Movie','id','RESTRICT','RESRICT');
            // create table
            $this->forge->createtable('session', TRUE);
        }

        //create price table
        if (!$this->db->tableexists('price'))
        {
            // Setup Keys
            $this->forge->addkey('id', TRUE);

            $this->forge->addfield(array(
                'id' => array('type' => 'INT', 'unsigned' => TRUE, 'null' => FALSE, 'auto_increment' => TRUE),
                'price_type' => array('type' => 'INT', 'unsigned' => TRUE),
                'value' => array('type' => 'INT', 'unsigned' => TRUE),
                'session_id' => array('type' => 'INT', 'unsigned' => TRUE)

            ));
            $this->forge->addForeignKey('session_id','session','id','RESTRICT','RESRICT');
            // create table
            $this->forge->createtable('price', TRUE);
        }

        //create seat table
        if (!$this->db->tableexists('seat'))
        {
            // Setup Keys
            $this->forge->addkey('id', TRUE);

            $this->forge->addfield(array(
                'id' => array('type' => 'INT', 'unsigned' => TRUE, 'null' => FALSE, 'auto_increment' => TRUE),
                'row' => array('type' => 'INT', 'unsigned' => TRUE),
                'number' => array('type' => 'INT', 'unsigned' => TRUE),
                'price_type' => array('type' => 'INT', 'unsigned' => TRUE),
                'zal_id' => array('type' => 'INT', 'unsigned' => TRUE)

            ));
            $this->forge->addForeignKey('zal_id','Zal','id','RESTRICT','RESRICT');
            // create table
            $this->forge->createtable('seat', TRUE);
        }

        //create ticket
        if (!$this->db->tableexists('ticket'))
        {
            // Setup Keys
            $this->forge->addkey('id', TRUE);

            $this->forge->addfield(array(
                'id' => array('type' => 'INT', 'unsigned' => TRUE, 'null' => FALSE, 'auto_increment' => TRUE),
                'session_id' => array('type' => 'INT', 'unsigned' => TRUE),
                'seat_id' => array('type' => 'INT', 'unsigned' => TRUE),
                'fullName' => array('type' => 'VARCHAR', 'constraint' => '255', 'null' => FALSE),

            ));
            $this->forge->addForeignKey('session_id','session','id','RESTRICT','RESRICT');
            $this->forge->addForeignKey('seat_id','seat','id','RESTRICT','RESRICT');

            // create table
            $this->forge->createtable('ticket', TRUE);
        }

    }

    //--------------------------------------------------------------------

    public function down()
    {
        //delete tables
        $this->forge->droptable('Movie');
        $this->forge->droptable('Zal');
        $this->forge->droptable('seat');
        $this->forge->droptable('session');
        $this->forge->droptable('ticket');
        $this->forge->droptable('price');

    }
}
