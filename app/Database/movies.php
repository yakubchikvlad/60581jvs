<?php namespace App\Database\Seeds;

use CodeIgniter\Database\Seeder;

class Movies extends Seeder
{
	public function run()
	{
        //seeding Movie table
       $data = [
                
                'name' => 'Титаник',
                'lasting' => '02:40:28',
        ];
        $this->db->table('Movie')->insert($data);

        $data = [

            'name' => 'Мстители',
            'lasting' => '02:30:21',
        ];
        $this->db->table('Movie')->insert($data);

        $data = [

            'name' => 'Бойцовский клуб',
            'lasting' => '02:15:57',
        ];
        $this->db->table('Movie')->insert($data);

        $data = [

            'name' => 'Интерстеллар',
            'lasting' => '04:30:27',
        ];
        $this->db->table('Movie')->insert($data);

        $data = [

            'name' => 'Властелин колец: Две крепости',
            'lasting' => '02:30:21',
        ];
        $this->db->table('Movie')->insert($data);

        $data = [

            'name' => 'Движение вниз',
            'lasting' => '02:20:00',
        ];
        $this->db->table('Movie')->insert($data);

        //seeding Zal table--------------------------------------------------------

        $data = [

            'name' => 'Зал 2: 2D',
        ];
        $this->db->table('Zal')->insert($data);

        $data = [

            'name' => 'Зал 3: 3D',
        ];
        $this->db->table('Zal')->insert($data);

        $data = [

            'name' => 'Зал 1: 2D',
        ];
        $this->db->table('Zal')->insert($data);

        $data = [

            'name' => 'Зал 4: 3D',
        ];
        $this->db->table('Zal')->insert($data);

        $data = [

            'name' => 'Зал 5: IMAX',
        ];
        $this->db->table('Zal')->insert($data);

        //seeding session table----------------------------------------------------------------
        $data = [
            'date' => '2021-10-13 17:25:17',
            'zal_id' => 1,
            'movie_id' => 1,

        ];
        $this->db->table('session')->insert($data);

        $data = [

            'zal_id' => 2,
            'movie_id' => 4,
            'date' => '2021-10-13 17:30:17',
        ];
        $this->db->table('session')->insert($data);

        $data = [

            'zal_id' => 3,
            'movie_id' => 2,
            'date' => '2021-06-15 17:26:01',
        ];
        $this->db->table('session')->insert($data);

        $data = [

            'zal_id' => 4,
            'movie_id' => 6,
            'date' => '2021-12-23 17:26:16',
        ];
        $this->db->table('session')->insert($data);

        $data = [

            'zal_id' => 5,
            'movie_id' => 3,
            'date' => '2021-06-15 17:26:01',
        ];
        $this->db->table('session')->insert($data);

        $data = [

            'zal_id' => 1,
            'movie_id' => 5,
            'date' => '2021-06-10 17:26:45',
        ];
        $this->db->table('session')->insert($data);

        $data = [

            'zal_id' => 2,
            'movie_id' => 6,
            'date' => '2021-11-23 17:26:58',
        ];
        $this->db->table('session')->insert($data);

        $data = [

            'zal_id' => 3,
            'movie_id' => 1,
            'date' => '2021-10-20 17:27:10',
        ];
        $this->db->table('session')->insert($data);

        $data = [

            'zal_id' => 4,
            'movie_id' => 4,
            'date' => '2021-08-17 17:27:26',
        ];
        $this->db->table('session')->insert($data);

        //seeding price table-------------------------------------------
        $data = [

            'session_id' => 1,
            'price_type' => '200',
            'value' => '210',
        ];
        $this->db->table('price')->insert($data);

        $data = [

            'session_id' => 2,
            'price_type' => '250',
            'value' => '270',
        ];
        $this->db->table('price')->insert($data);

        $data = [

            'session_id' => 3,
            'price_type' => '250',
            'value' => '290',
        ];
        $this->db->table('price')->insert($data);

        $data = [

            'session_id' => 4,
            'price_type' => '200',
            'value' => '210',
        ];
        $this->db->table('price')->insert($data);

        $data = [

            'session_id' => 5,
            'price_type' => '400',
            'value' => '420',
        ];
        $this->db->table('price')->insert($data);

        $data = [

            'session_id' => 6,
            'price_type' => '400',
            'value' => '410',
        ];
        $this->db->table('price')->insert($data);

        $data = [

            'session_id' => 7,
            'price_type' => '600',
            'value' => '650',
        ];
        $this->db->table('price')->insert($data);

        $data = [

            'session_id' => 8,
            'price_type' => '400',
            'value' => '430',
        ];
        $this->db->table('price')->insert($data);

        $data = [

            'session_id' => 9,
            'price_type' => '400',
            'value' => '410',
        ];
        $this->db->table('price')->insert($data);

        $data = [

            'session_id' => 4,
            'price_type' => '200',
            'value' => '270',
        ];
        $this->db->table('price')->insert($data);

        $data = [

            'session_id' => 7,
            'price_type' => '600',
            'value' => '640',
        ];
        $this->db->table('price')->insert($data);

        $data = [

            'session_id' => 5,
            'price_type' => '400',
            'value' => '210',
        ];
        $this->db->table('price')->insert($data);

        $data = [

            'session_id' => 6,
            'price_type' => '200',
            'value' => '270',
        ];
        $this->db->table('price')->insert($data);

        //seeding seat table------------------------------------------
        $data = [

            'zal_id' => 1,
            'row' => '7',
            'number' => '8',
            'price_type' => '200',
        ];
        $this->db->table('seat')->insert($data);

        $data = [

            'zal_id' => 2,
            'row' => '5',
            'number' => '10',
            'price_type' => '400',
        ];
        $this->db->table('seat')->insert($data);

        $data = [

            'zal_id' => 2,
            'row' => '2',
            'number' => '2',
            'price_type' => '200',
        ];
        $this->db->table('seat')->insert($data);

        $data = [

            'zal_id' => 5,
            'row' => '9',
            'number' => '5',
            'price_type' => '200',
        ];
        $this->db->table('seat')->insert($data);

        $data = [

            'zal_id' => 3,
            'row' => '4',
            'number' => '3',
            'price_type' => '600',
        ];
        $this->db->table('seat')->insert($data);

        $data = [

            'zal_id' => 4,
            'row' => '1',
            'number' => '1',
            'price_type' => '200',
        ];
        $this->db->table('seat')->insert($data);

        $data = [

            'zal_id' => 1,
            'row' => '1',
            'number' => '2',
            'price_type' => '200',
        ];
        $this->db->table('seat')->insert($data);

        $data = [

            'zal_id' => 3,
            'row' => '3',
            'number' => '5',
            'price_type' => '210',
        ];
        $this->db->table('seat')->insert($data);

        $data = [

            'zal_id' => 2,
            'row' => '1',
            'number' => '10',
            'price_type' => '200',
        ];
        $this->db->table('seat')->insert($data);

        $data = [

            'zal_id' => 1,
            'row' => '2',
            'number' => '1',
            'price_type' => '200',
        ];
        $this->db->table('seat')->insert($data);

        $data = [

            'zal_id' => 5,
            'row' => '2',
            'number' => '2',
            'price_type' => '200',
        ];
        $this->db->table('seat')->insert($data);

        $data = [

            'zal_id' => 4,
            'row' => '1',
            'number' => '5',
            'price_type' => '200',
        ];
        $this->db->table('seat')->insert($data);

        $data = [

            'zal_id' => 3,
            'row' => '1',
            'number' => '10',
            'price_type' => '200',
        ];
        $this->db->table('seat')->insert($data);

        $data = [

            'zal_id' => 2,
            'row' => '3',
            'number' => '1',
            'price_type' => '200',
        ];
        $this->db->table('seat')->insert($data);

        $data = [

            'zal_id' => 1,
            'row' => '3',
            'number' => '5',
            'price_type' => '250',
        ];
        $this->db->table('seat')->insert($data);

        $data = [

            'zal_id' => 2,
            'row' => '3',
            'number' => '7',
            'price_type' => '200',
        ];
        $this->db->table('seat')->insert($data);

        $data = [

            'zal_id' => 3,
            'row' => '4',
            'number' => '1',
            'price_type' => '250',
        ];
        $this->db->table('seat')->insert($data);

        $data = [

            'zal_id' => 4,
            'row' => '1',
            'number' => '1',
            'price_type' => '200',
        ];
        $this->db->table('seat')->insert($data);

        $data = [

            'zal_id' => 2,
            'row' => '1',
            'number' => '5',
            'price_type' => '200',
        ];
        $this->db->table('seat')->insert($data);

        $data = [

            'zal_id' => 2,
            'row' => '1',
            'number' => '10',
            'price_type' => '200',
        ];
        $this->db->table('seat')->insert($data);

        $data = [

            'zal_id' => 2,
            'row' => '3',
            'number' => '1',
            'price_type' => '200',
        ];
        $this->db->table('seat')->insert($data);

        $data = [

            'zal_id' => 2,
            'row' => '3',
            'number' => '5',
            'price_type' => '250',
        ];
        $this->db->table('seat')->insert($data);

        $data = [

            'zal_id' => 2,
            'row' => '4',
            'number' => '10',
            'price_type' => '250',
        ];
        $this->db->table('seat')->insert($data);


        //seeding ticket table--------------------------------------------
        $data = [

            'session_id' => 1,
            'seat_id' => 12,
            'fullName' => 'Жупель Владимир Владимирович',
        ];
        $this->db->table('ticket')->insert($data);

        $data = [

            'session_id' => 4,
            'seat_id' => 13,
            'fullName' => 'Гитич Ольга Николаевна',
        ];
        $this->db->table('ticket')->insert($data);
        $data = [

            'session_id' => 4,
            'seat_id' => 17,
            'fullName' => 'Розембаум Игорь Яковлевич',
        ];
        $this->db->table('ticket')->insert($data);
        $data = [

            'session_id' => 8,
            'seat_id' => 3,
            'fullName' => 'Вандам Олег Жанклодович',
        ];
        $this->db->table('ticket')->insert($data);
        $data = [

            'session_id' => 6,
            'seat_id' => 12,
            'fullName' => 'Джигурда Ольга Рахитовна',
        ];
        $this->db->table('ticket')->insert($data);
        $data = [

            'session_id' => 8,
            'seat_id' => 7,
            'fullName' => 'Перельман Андрей Андреевич',
        ];
        $this->db->table('ticket')->insert($data);
        $data = [

            'session_id' => 5,
            'seat_id' => 2,
            'fullName' => 'Эрнст Константин Константинович',
        ];
        $this->db->table('ticket')->insert($data);
        $data = [

            'session_id' => 8,
            'seat_id' => 3,
            'fullName' => 'Кутаков Денис Раисович',
        ];
        $this->db->table('ticket')->insert($data);
        $data = [

            'session_id' => 2,
            'seat_id' => 18,
            'fullName' => 'Пережогин Даниил Евгеньевич',
        ];
        $this->db->table('ticket')->insert($data);

        $data = [

            'session_id' => 2,
            'seat_id' => 17,
            'fullName' => 'Денастьев Олег Николаевич',
        ];
        $this->db->table('ticket')->insert($data);

        $data = [

            'session_id' => 8,
            'seat_id' => 10,
            'fullName' => 'Толстой Лев Николаевич',
        ];
        $this->db->table('ticket')->insert($data);

        $data = [

            'session_id' => 7,
            'seat_id' => 16,
            'fullName' => 'Покатушев Андрей Георгиевич',
        ];
        $this->db->table('ticket')->insert($data);

        $data = [

            'session_id' => 4,
            'seat_id' => 22,
            'fullName' => 'Катлованов Платон Андреевич',
        ];
        $this->db->table('ticket')->insert($data);

        $data = [

            'session_id' => 8,
            'seat_id' => 17,
            'fullName' => 'Крузенштерн Пароход Советович',
        ];
        $this->db->table('ticket')->insert($data);

        $data = [

            'session_id' => 4,
            'seat_id' => 5,
            'fullName' => 'Бакуган Дмитрий Николаевич',
        ];
        $this->db->table('ticket')->insert($data);

        $data = [

            'session_id' => 9,
            'seat_id' => 20,
            'fullName' => 'Перепитов Александр Константинович',
        ];
        $this->db->table('ticket')->insert($data);


	}
}
