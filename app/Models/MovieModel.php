<?php namespace App\Models;
use CodeIgniter\Model;
class MovieModel extends Model
{
    protected $table = 'Movie'; //таблица, связанная с моделью


    protected $allowedFields = ['name', 'lasting', 'poster_url'];

    // Если $user_id == null то возвращаются все рейтинги, иначе - только принадлежащие пользователю с user_id
    public function getMovies($id = null, $search = '', $per_page = null)
    {
        $model = $this->like('name', is_null($search) ? '' : $search, 'both');
        if (!is_null($id)) {
            $model = $model->where('id', $id);
        }
        // Пагинация
        return $model->paginate($per_page, 'group1');
    }

    public function getMovie($id = null)
    {
        if (!isset($id)) {
            return $this->findAll();
        }
        return $this->where(['id' => $id])->first();
    }

    public function getMovieWithSessions($id = null, $search = '')
    {
        $builder = $this->select('*')->join('session','Movie.id = session.movie_id')->like('name', $search,'both', null, true)->orlike('zal_id',$search,'both',null,true);
        if (!is_null($id))
        {
            return $builder->where(['Movie.id' => $id])->first();
        }
        //return $builder->findAll();
        return $builder;
    }
}
