<?php

namespace App\Services;

use OAuth2\GrantType\UserCredentials;
use OAuth2\Server;
use OAuth2\Request;
use App\Models\OAuthModel;

class OAuth
{
    public $server;

    function __construct()
    {
        $this->init();
    }

    public function init()
    {
        $storage = new MyPdo(array('dsn' => 'pgsql:host=ec2-52-17-1-206.eu-west-1.compute.amazonaws.com;port=5432;dbname=d9bpqbr49028tn;user=ylohxfyieukgnu;password=66bef1e9404f730c9053726697f832ec46aa24cc1b5ca7d1ea413f7c96f50aad'),array('user_table' => 'users'));
        $grantType = new UserCredentials($storage);
        $this->server = new Server($storage);
        $this->server->addGrantType($grantType);
    }

    public function isLoggedIn()
    {
        return $this->server->verifyResourceRequest(Request::createFromGlobals());
    }

}
