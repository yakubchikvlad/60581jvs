<?php

namespace Config;

use CodeIgniter\Database\Config;

/**
 * Database Configuration
 */
class Database extends Config
{
    /**
     * The directory that holds the Migrations
     * and Seeds directories.
     *
     * @var string
     */
    public $filesPath = APPPATH . 'Database' . DIRECTORY_SEPARATOR;

    /**
     * Lets you choose which connection group to
     * use if no other is specified.
     *
     * @var string
     */
    public $defaultGroup = 'default';

    /**
     * The default database connection.
     *
     * @var array
     */
    //pgsql:host=ec2-3-248-103-75.eu-west-1.compute.amazonaws.com;port=5432;dbname=d7e5rm5h85655v;user=jhpdbogvljnuek;password=ce57a9d5a5559bff8dee91dabf3b35d68609900088c7ed54a8861c2627989d90
    public $default = [
        'DSN'      => 'pgsql:host=ec2-52-17-1-206.eu-west-1.compute.amazonaws.com;port=5432;dbname=d9bpqbr49028tn;user=ylohxfyieukgnu;password=66bef1e9404f730c9053726697f832ec46aa24cc1b5ca7d1ea413f7c96f50aad',
        'hostname' => '',//'ec2-3-248-103-75.eu-west-1.compute.amazonaws.com',
        'username' => '',//'jhpdbogvljncuek',
        'password' => '',//'ce57a9d5a5559bff8dee91dabf3b35d68609900088c7ed54a8861c2627989d90',
        'database' => '',//'d7e5rm5h85655v',
        'DBDriver' => 'Postgre',//'MySQLi',
        'DBPrefix' => '',
        'pConnect' => false,
        'DBDebug'  => (ENVIRONMENT !== 'production'),
        'charset'  => 'utf8',
        'DBCollat' => 'utf8_general_ci',
        'swapPre'  => '',
        'encrypt'  => false,
        'compress' => false,
        'strictOn' => false,
        'failover' => [],
        'port'     => 5432,
    ];

    /**
     * This database connection is used when
     * running PHPUnit database tests.
     *
     * @var array
     */
    public $tests = [
        'DSN'      => '',
        'hostname' => '127.0.0.1',
        'username' => '',
        'password' => '',
        'database' => ':memory:',
        'DBDriver' => 'SQLite3',
        'DBPrefix' => 'db_',  // Needed to ensure we're working correctly with prefixes live. DO NOT REMOVE FOR CI DEVS
        'pConnect' => false,
        'DBDebug'  => (ENVIRONMENT !== 'production'),
        'charset'  => 'utf8',
        'DBCollat' => 'utf8_general_ci',
        'swapPre'  => '',
        'encrypt'  => false,
        'compress' => false,
        'strictOn' => false,
        'failover' => [],
        'port'     => 3306,
    ];

    public function __construct()
    {
        parent::__construct();

        // Ensure that we always set the database group to 'tests' if
        // we are currently running an automated test suite, so that
        // we don't overwrite live data on accident.
        if (ENVIRONMENT === 'testing') {
            $this->defaultGroup = 'tests';
        }
    }
}
