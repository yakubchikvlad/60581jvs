<!DOCTYPE html>
<head>
    <title>Кинотеатр Заря</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <script src="https://kit.fontawesome.com/6e9b058a28.js"></script>
    <meta charset="utf-8"><meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <style>
        main{
            margin-top: 100px;
        }
    </style>
</head>
<body>
<nav class="navbar navbar-dark bg-dark fixed-top">
    <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle active"  style="color:white;" href="#" id="dropdown01" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">Фильмы
        </a>
        <div class="dropdown-menu" aria-labelledby="dropdown01">
            <a class="dropdown-item" href="<?= base_url()?>/movie">Все фильмы</a>
            <a class="dropdown-item" href="<?= base_url()?>/movie/store">Добавить фильм</a>
        </div>
    </li>

    <a class="navbar-brand fa fas fa-film fa-4x" style="color:red;" href="<?= base_url()?>">Кинотеатр Заря</a>



	<?php if (! $ionAuth->loggedIn()): ?>
            <div class="nav-item dropdown">
              <a class="nav-link active" href="<?= base_url()?>/auth/login"><span class="fas fa fa-sign-in-alt" style="color:white"></span>&nbsp;&nbsp;Вход</a>
            </div>
        <?php else: ?>
            <li class="nav-item dropdown" style="color:white">
                <a class="nav-link dropdown-toggle active" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true" style="color:white"><span class="fas fa fa-user-alt" style="color:white"></span>&nbsp;&nbsp;  <?php echo $ionAuth->user()->row()->email; ?></a>
                <div class="dropdown-menu" aria-labelledby="dropdown01">
                    <a class="dropdown-item" href="<?= base_url()?>/auth/logout"><span class="fas fa fa-sign-in-alt" style="color:white"></span>&nbsp;&nbsp;Выход</a>
                </div>
            </li>
        <?php endif ?>



</nav>

<main role="main">

    <?php if (session()->getFlashdata('message')) :?>
        <div class="alert alert-info" role="alert" style="max-width: 540px;">
            <?= session()->getFlashdata('message') ?>
        </div>
    <?php endif ?>

    <?= $this->renderSection('content') ?>
</main>
<footer class="text-center">
    <p>© Якубчик В.С. 2021&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="/pages/view/agreement">Пользовательское соглашение</a></p>
</footer>
<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ho+j7jyWK8fNQe+A12Hb8AhRq26LrZ/JpcUGGOn+Y7RsweNrtN/tE3MoK7ZeZDyx" crossorigin="anonymous"></script>
</body>
</html>
