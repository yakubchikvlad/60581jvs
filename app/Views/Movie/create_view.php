<?= $this->extend('templates/layout') ?>
<?= $this->section('content') ?>
    <div class="container" style="max-width: 540px;">

        <?= form_open_multipart('Movie/store'); ?>
        <div class="form-group">
            <label for="name">Имя</label>
            <input type="text" class="form-control <?= ($validation->hasError('name')) ? 'is-invalid' : ''; ?>" name="name"
                   value="<?= old('name'); ?>">
            <div class="invalid-feedback">
                <?= $validation->getError('name') ?>
            </div>



        </div>
        <div class="form-group">
            <label for="lasting">Длительность фильма</label>
            <input type="time" class="form-control <?= ($validation->hasError('lasting')) ? 'is-invalid' : ''; ?>" name="lasting" value="<?= old('lasting'); ?>">
            <div class="invalid-feedback">
                <?= $validation->getError('lasting') ?>
            </div>
        </div>

        <div class="form-group">
            <label for="poster">Постер к фильму</label>
            <input type="file" class="form-control-file <?= ($validation->hasError('picture')) ? 'is-invalid' : ''; ?>" name="picture">
            <div class="invalid-feedback">
                <?= $validation->getError('picture') ?>
            </div>
        </div>

        <div class="form-group">
            <button type="submit" class="btn btn-primary" name="submit">Создать</button>
        </div>
    </div>
        </form>


    </div>
<?= $this->endSection() ?>