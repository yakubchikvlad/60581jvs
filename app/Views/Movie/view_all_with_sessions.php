<?= $this->extend('templates/layout') ?>
<?= $this->section('content') ?>
<?php use CodeIgniter\I18n\Time; ?>
    <div class="container main">
        <?php if (!empty($movie) && is_array($movie)) : ?>
            <h2>Все фильмы с сеансами:</h2>
            <div class="d-flex mb-2">
                <?= $pager->links('group1','my_page') ?>
                <?= form_open('Movie/viewAllWithSessions', ['style' => 'display: flex']); ?>
                <select name="per_page" class="ml-3" aria-label="per_page">
                    <option value="2" <?php if($per_page == '2') echo("selected"); ?>>2</option>
                    <option value="5"  <?php if($per_page == '5') echo("selected"); ?>>5</option>
                    <option value="10" <?php if($per_page == '10') echo("selected"); ?>>10</option>
                    <option value="20" <?php if($per_page == '20') echo("selected"); ?>>20</option>
                </select>
                <button class="btn btn-dark " type="submit" class="btn btn-dark ">На странице</button>
                </form>
                <?= form_open('Movie/viewAllWithSessions',['style' => 'display: flex']); ?>
                <input type="text" class="form-control ml-3" name="search" placeholder="Имя или номер зала" aria-label="Search"
                       value="<?= $search; ?>">
                <button class="btn btn-dark " type="submit" class="btn btn-dark ">Найти</button>
                </form>
            </div>
            <table class="table table-striped">
                <thead>
                <th scope="col">Постер</th>
                <th scope="col">Название</th>
                <th scope="col">Номер Зала</th>
                <th scope="col">Дата начала сеанса</th>
                <th scope="col">Доступные действия</th>
                </thead>
                <tbody>
                <?php foreach ($movie as $item): ?>
                    <tr>
                        <td>
                                <img height="50" src="https://cdn-icons-png.flaticon.com/512/72/72279.png" alt="<?= esc($item['name']); ?>">
                        </td>
                        <td><?= esc($item['name']); ?></td>
                        <td><?= esc($item['zal_id']); ?></td>
                        <td><?= esc(Time::parse($item['date'])->toDateTimeString() ); ?></td>
                        <td>
                            <a href="<?= base_url()?>/Movie/view/<?= esc($item['movie_id']); ?>" class="btn btn-dark btn-lg btn-sm">Просмотреть</a>
                        </td>

                    </tr>
                <?php endforeach; ?>
                </tbody>
            </table>

        <?php else : ?>
            <div class="text-center">
                <p>Рейтинги не найдены </p>
                <a class="btn btn-primary btn-lg" href="<?= base_url()?>/rating/create"><span class="fas fa-tachometer-alt" style="color:white"></span>&nbsp;&nbsp;Создать рейтинг</a>
            </div>
        <?php endif ?>
    </div>
<?= $this->endSection() ?>