<?= $this->extend('templates/layout') ?>
<?= $this->section('content') ?>
<div class="container main">
<h2 style="text-align: center">Все фильмы</h2>


<?php if (!empty($movie) && is_array($movie)) : ?>

    <?php foreach ($movie as $item): ?>

        <div class="card mb-3 text" style="max-width: 540px;">
           <div class="row">
                <div class="col-md-4 d-flex align-items-center">
                    <img height="150" src="<?=($item['poster_url']);?>" class="card-img" alt="<?= esc($item['name']); ?>">
                </div>
                <div class="col-md-8">
                    <div class="card-body">
                        <h5 class="card-title"><?= esc($item['name']); ?></h5>
                        <a href="/Movie/view/<?= esc($item['id']); ?>/" class="btn btn-dark">Просмотреть</a>
                        <p class="card-text"><small class="text-muted">Фильм</small></p>
                    </div>
                </div>
            </div>
        </div>


    <?php endforeach; ?>
<?php else : ?>
    <p>Невозможно найти фильмы.</p>
<?php endif ?>
</div>
<?= $this->endSection() ?>
