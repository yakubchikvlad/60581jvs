<?= $this->extend('templates/layout') ?>
<?= $this->section('content') ?>
<div class="container main">
    <?php use CodeIgniter\I18n\Time; ?>
    <?php if (!empty($movie)) : ?>
            <div class="card mb-3" style="max-width: 540px;">
                <div class="row">
                    <div class="col-md-4 d-flex align-items-center">
                         <img height="150" src="<?= esc($movie['poster_url']); ?>" class="card-img" alt="<?= esc($movie['name']); ?>">
                    </div>
                    <div class="col-md-8">
                        <div class="card-body">
                            <h5 class="card-title"><?= esc($movie['name']); ?></h5>
                            <div class="d-flex justify-content-between">
                                <div class="my-0">Длительность:</div>
                                <div class="text-muted"><?= esc(Time::parse($movie['lasting'])->toTimeString() ); ?></div>
                                <a href="/Movie/edit/<?= esc($movie['id']); ?>/" class="btn btn-dark">Изменить</a>
                                <div>
                                <a href="/Movie/delete/<?= esc($movie['id']); ?>/" class="btn btn-dark">X</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
    <?php else : ?>
        <p>Фильм не найден.</p>
    <?php endif ?>
</div>
<?= $this->endSection() ?>
