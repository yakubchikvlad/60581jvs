<?php namespace App\Controllers;

use App\Models\MovieModel;
use Aws\S3\S3Client;

class Movie extends BaseController

{

    public function __construct()
    {
        //parent::__construct();

        Header('Access-Control-Allow-Origin: *'); //for allow any domain, insecure
        Header('Access-Control-Allow-Headers: *'); //for allow any headers, insecure
        Header('Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE'); //method
    }
 public function index() //Обображение всех записей
 {
//если пользователь не аутентифицирован - перенаправление на страницу входа

        if (!$this->ionAuth->loggedIn())
        {
            return redirect()->to('/auth/login');
        }

   $model = new MovieModel();
   $data ['movie'] = $model->getMovie();
   echo view('Movie/view_all', $this->withIon($data));
 }

 public function view($id = null) //отображение одной записи
 {
        //если пользователь не аутентифицирован - перенаправление на страницу входа
        if (!$this->ionAuth->loggedIn())
        {
            return redirect()->to('/auth/login');
        }

   $model = new MovieModel();
   $data ['movie'] = $model->getMovie($id);
   echo view('Movie/view', $this->withIon($data));
 }

 public function viewAllWithSessions()
    {
        if ($this->ionAuth->isAdmin())
        {
            //Подготовка значения количества элементов выводимых на одной странице
            if (!is_null($this->request->getPost('per_page'))) //если кол-во на странице есть в запросе
            {
                //сохранение кол-ва страниц в переменной сессии
                session()->setFlashdata('per_page', $this->request->getPost('per_page'));
                $per_page = $this->request->getPost('per_page');
            }
            else {
                $per_page = session()->getFlashdata('per_page');
                session()->setFlashdata('per_page', $per_page); //пересохранение в сессии
                if (is_null($per_page)) $per_page = '5'; //кол-во на странице по умолчанию
            }
            $data['per_page'] = $per_page;

            //Обработка запроса на поискa
            if (!is_null($this->request->getPost('search')))
            {
                session()->setFlashdata('search', $this->request->getPost('search'));
                $search = $this->request->getPost('search');
            }
            else {
                $search = session()->getFlashdata('search');
                session()->setFlashdata('search', $search);
                if (is_null($search)) $search = '';
            }
            $data['search'] = $search;

            helper(['form','url']);

            $model = new MovieModel();
            $data['movie'] = $model->getMovieWithSessions(null, $search)->paginate($per_page, 'group1');
            $data['pager'] = $model->pager;
            echo view('Movie/view_all_with_sessions', $this->withIon($data));
        }
        else
        {
            session()->setFlashdata('message', lang('Movie.admin_permission_needed'));
            return redirect()->to('/auth/login');
        }
    }

    public function create()
    {
        if (!$this->ionAuth->loggedIn())
        {
            return redirect()->to('/auth/login');
        }
        helper(['form']);
        $data ['validation'] = \Config\Services::validation();
        echo view('Movie/create_view', $this->withIon($data));
    }

    public function store()
    {
        helper(['form','url']);

        if ($this->request->getMethod() === 'post' && $this->validate([
                'name' => 'required|min_length[3]|max_length[255]',
                'lasting'  => 'required',
                'picture'  => 'is_image[picture]|max_size[picture,12000]',
            ]))
        {
            $insert = null;

            $file = $this->request->getFile('picture');
            if ($file->getSize() != 0) {

                //подключение хранилища
                
                $s3 = new S3Client([
                    'version' => 'latest',
                    'region' => 'us-east-1',
                    'endpoint' => getenv('S3_ENDPOINT'),
                    'use_path_style_endpoint' => true,
                    'credentials' => [
                        'key' => getenv('S3_KEY'), //чтение настроек окружения из файла .env
                        'secret' => getenv('S3_SECRET'), //чтение настроек окружения из файла .env
                    ],
                ]);


                //получение расширения имени загруженного файла
                $ext = explode('.', $file->getName());
                $ext = $ext[count($ext) - 1];

                //загрузка файла в хранилище
                $insert = $s3->putObject([
                    'Bucket' => getenv('S3_BUCKET'), //чтение настроек окружения из файла .env
                    //генерация случайного имени файла
                    'Key' => getenv('S3_KEY') . '/file' . rand(100000, 999999) . '.' . $ext,
                    'Body' => fopen($file->getRealPath(), 'r+'),
                    //'@use_path_style_endpoint' => true

                ]);

            }

            $model = new MovieModel();

            $new_data = [
                'name' => $this->request->getPost('name'),
                'lasting' => $this->request->getPost('lasting'),
            ];

            if (!is_null($insert))
                $new_data['poster_url'] = $insert['ObjectURL'];
            else{
                session()->setFlashdata('message', lang('Не получена ссылка'));
                $model->save($new_data);
                return redirect()->to('/Movie');
            }

            $model->save($new_data);
            session()->setFlashdata('message', lang('Запись успешно добавлена.'));
            return redirect()->to('/Movie');
        }
        else
        {
            return redirect()->to('Movie/create')->withInput();
        }
    }

    public function edit($id)
    {
        if (!$this->ionAuth->loggedIn())
        {
            return redirect()->to('/auth/login');
        }
        $model = new MovieModel();

        helper(['form']);
        $data ['movie'] = $model->getMovie($id);
        $data ['validation'] = \Config\Services::validation();
        echo view('Movie/edit', $this->withIon($data));

    }

    public function update()
    {

        helper(['form','url']);
        echo 'Movie/edit/'.$this->request->getPost('id');

        if ($this->request->getMethod() === 'post' && $this->validate([
                'id'  => 'required',
                'name' => 'required|min_length[3]|max_length[255]',
                'lasting'  => 'required',
            ]))
        {
            //
            $insert = null;

            $file = $this->request->getFile('picture');
            if ($file->getSize() != 0) {

                //подключение хранилища

                $s3 = new S3Client([
                    'version' => 'latest',
                    'region' => 'us-east-1',
                    'endpoint' => getenv('S3_ENDPOINT'),
                    'use_path_style_endpoint' => true,
                    'credentials' => [
                        'key' => getenv('S3_KEY'), //чтение настроек окружения из файла .env
                        'secret' => getenv('S3_SECRET'), //чтение настроек окружения из файла .env
                    ],
                ]);


                //получение расширения имени загруженного файла
                $ext = explode('.', $file->getName());
                $ext = $ext[count($ext) - 1];

                //загрузка файла в хранилище
                $insert = $s3->putObject([
                    'Bucket' => getenv('S3_BUCKET'), //чтение настроек окружения из файла .env
                    //генерация случайного имени файла
                    'Key' => getenv('S3_KEY') . '/file' . rand(100000, 999999) . '.' . $ext,
                    'Body' => fopen($file->getRealPath(), 'r+'),
                    '@use_path_style_endpoint' => true

                ]);

            }

            $model = new MovieModel();

            $new_data = [
                'id' => $this->request->getPost('id'),
                'name' => $this->request->getPost('name'),
                'lasting' => $this->request->getPost('lasting'),
            ];

            if (!is_null($insert))
                $new_data['poster_url'] = $insert['ObjectURL'];
            else{
                session()->setFlashdata('message', lang('Не получена ссылка'));
                $model->save($new_data);
                return redirect()->to('/Movie');
            }

            $model->save($new_data);

            session()->setFlashdata('message', lang('Запись успешно обновлена.'));

            return redirect()->to('Movie');
        }
        else
        {
            return redirect()->to('Movie/edit/'.$this->request->getPost('id'))->withInput();
        }
    }

    public function delete($id)
    {
        if (!$this->ionAuth->loggedIn())
        {
            return redirect()->to('/auth/login');
        }
        $model = new MovieModel();
        $model->delete($id);

        session()->setFlashdata('message', lang('Запись была удалена'));
        return redirect()->to('/Movie');
    }
}

