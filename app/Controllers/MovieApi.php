<?php

namespace App\Controllers;

use CodeIgniter\RESTful\ResourceController;
use App\Services\OAuth;
use App\Models\OAuthModel;
use OAuth2\Request;
use App\Models\MovieModel;
use Aws\S3\S3Client;

class MovieApi extends ResourceController
{
    protected $modelName = 'App\Models\MovieModel';
    protected $format = 'json';
    protected $oauth;

    public function __construct()
    {
        //parent::__construct();

        Header('Access-Control-Allow-Origin: *'); //for allow any domain, insecure
        Header('Access-Control-Allow-Headers: *'); //for allow any headers, insecure
        Header('Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE'); //method
    }

    public function movie() //Отображение всех записей
    {
        /*$oauth = new OAuth();
        if ($oauth->isLoggedIn()) {
            return $this->respond($this->model->getMovie());

        }
        $oauth->server->getResponse()->send();
        */
        $this->oauth = new OAuth();
        if ($this->oauth->isLoggedIn()) {
            $OAuthModel = new OAuthModel();
            $model = $this->model;
            $per_page = $this->request->getPost('per_page');
            $search = $this->request->getPost('search');
            $data = $this->model->getMovies($OAuthModel->getUser()->group_name == 'admin' ? null : $OAuthModel->getUser()->id, $search, $per_page);
            //Ответ контроллера включает данные (ratings) и параметры пагинации (pager)
            return $this->respond(['movie' => $data, 'pager' => $model->pager->getDetails('group1')]);
        } else $this->oauth->server->getResponse()->send();
    }

    public function store()
    {
        $this->oauth = new OAuth();
        if ($this->oauth->isLoggedIn()) {
            //$OAuthModel = new OAuthModel();
            $model = $this->model;

            //$model = new MovieModel();

            //['name', 'lasting', 'poster_url'];

            if ($this->request->getMethod() === 'post' && $this->validate([
                    'name' => 'required|min_length[3]|max_length[255]',
                    'lasting'  => 'required',
                    'picture'  => 'is_image[picture]|max_size[picture,12000]',
                ]))
            {
                $insert = null;

                $file = $this->request->getFile('picture');
                if ($file->getSize() != 0) {

                    //подключение хранилища

                    $s3 = new S3Client([
                        'version' => 'latest',
                        'region' => 'us-east-1',
                        'endpoint' => getenv('S3_ENDPOINT'),
                        'use_path_style_endpoint' => true,
                        'credentials' => [
                            'key' => getenv('S3_KEY'), //чтение настроек окружения из файла .env
                            'secret' => getenv('S3_SECRET'), //чтение настроек окружения из файла .env
                        ],
                    ]);


                    //получение расширения имени загруженного файла
                    $ext = explode('.', $file->getName());
                    $ext = $ext[count($ext) - 1];

                    //загрузка файла в хранилище
                    $insert = $s3->putObject([
                        'Bucket' => getenv('S3_BUCKET'), //чтение настроек окружения из файла .env
                        //генерация случайного имени файла
                        'Key' => getenv('S3_KEY') . '/file' . rand(100000, 999999) . '.' . $ext,
                        'Body' => fopen($file->getRealPath(), 'r+'),
                        //'@use_path_style_endpoint' => true

                    ]);

                }



                $new_data = [
                    'name' => $this->request->getPost('name'),
                    'lasting' => $this->request->getPost('lasting'),
                ];

                if (!is_null($insert))
                    $new_data['poster_url'] = $insert['ObjectURL'];
                else{
                    $model->save($new_data);
                    return $this->respondCreated(null, 'S3 Link error. Empty poster url. Movie created successfully');
                }

                $model->save($new_data);
                //-----------------------------
                return $this->respondCreated(null, 'Movie created successfully');
            } else {
                return $this->respond($this->validator->getErrors());
            }
        } else $this->oauth->server->getResponse()->send();
    }

    public function delete($id = null)
    {
        $this->oauth = new OAuth();
        if ($this->oauth->isloggedIn()) {

            $model = new MovieModel();
            $model->delete($id);
            return $this->respondDeleted(null, 'Movie deleted successfully');
        }

        $this->oauth->server->getResponse()->send();
    }

}
